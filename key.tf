
resource "tls_private_key" "keypair" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "pkey" {
  filename = "id_rsa.pub"
  content = tls_private_key.keypair.public_key_pem
}

resource "local_file" "prkey" {
  filename = "id_rsa"
  content = tls_private_key.keypair.private_key_pem
}

resource "aws_key_pair" "piholekey" {
  key_name   = "pihole-key"
  public_key = tls_private_key.keypair.public_key_openssh
}