
variable "myip" {
  type = string
}

variable "dns_zone_id" {
  type = string
}

variable "dns_hostname" {
  type = string
}

variable "cert_email" {
  type = string
}