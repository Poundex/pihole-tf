module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "3.14.2"

  name = "pihole"
  cidr = "10.90.0.0/16"

  azs             = ["eu-west-2a", "eu-west-2b", "eu-west-2c"]
  private_subnets = ["10.90.1.0/24", "10.90.2.0/24", "10.90.3.0/24"]
  public_subnets  = ["10.90.101.0/24", "10.90.102.0/24", "10.90.103.0/24"]

  enable_nat_gateway = false
  enable_vpn_gateway = false

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}

module "sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "segrme"
  vpc_id      = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      rule        = "all-all"
      cidr_blocks = "${var.myip}/32"
    },
    {
      rule = "all-all",
      cidr_blocks = module.vpc.vpc_cidr_block
    }
  ]
  egress_with_cidr_blocks = [
    {
      rule = "all-all",
      cidr_blocks = "0.0.0.0/0"
    }
  ]
}