
module "k3s" {
  depends_on = [local_file.prkey, module.ec2_instance]
  
  source  = "xunleii/k3s/module"
  k3s_version = "v1.24.1+k3s1"

  global_flags = [
    "--tls-san ${aws_eip.eip.public_ip}",
    "--write-kubeconfig-mode 644"
  ]

  generate_ca_certificates = false
  use_sudo = true
  
  servers = {
    server-one = {
      ip         = module.ec2_instance.private_ip
      connection = {
        host = aws_eip.eip.public_ip
        user = "ubuntu"
        private_key = tls_private_key.keypair.private_key_pem
      }
    }
  }
}

resource "null_resource" "fetch_kubeconfig" {
  depends_on = [module.k3s]
  triggers = {
    everytime = md5(timestamp())
  }

  provisioner "remote-exec" {
    inline = ["sh -c -- \"sudo cp /etc/rancher/k3s/k3s.yaml /tmp/kubeconfig; sudo chmod a+r /tmp/kubeconfig\""]
  }

  provisioner "local-exec" {
    command = "scp -i ${local_file.prkey.filename} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@${aws_eip.eip.public_ip}:/tmp/kubeconfig kubeconfig"
  }

  provisioner "local-exec" {
    command = "sed \"s/127.0.0.1/${aws_eip.eip.public_ip}/g\" -i kubeconfig"
  }

  provisioner "remote-exec" {
    inline = ["sh -c -- \"sudo rm -f /tmp/kubeconfig\""]
  }

  connection {
    type = "ssh"
    host = aws_eip.eip.public_ip
    user = "ubuntu"
    private_key = file(local_file.prkey.filename)
  }
}