
output "piholepw" {
  value = nonsensitive(random_password.piholepw.result)
}

output "ip" {
  value = aws_eip.eip.public_ip
}