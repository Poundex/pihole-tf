
resource "kubernetes_namespace" "piholens" {
  depends_on = [null_resource.fetch_kubeconfig]
  metadata {
    name = "pihole"
  }
}

resource "helm_release" "pihole" {
  depends_on = [null_resource.fetch_kubeconfig, kubernetes_namespace.piholens, kubernetes_secret.piholepw]
  name  = "pihole"
  repository = "https://gitlab.com/api/v4/projects/37344622/packages/helm/stable"
  chart = "pihole-cloud"
  namespace = "pihole"
  atomic = true
  version = "0.1.0"
  
  set {
    name  = "cert.cert"
    value = "${local_file.certificate_pem.content}${local_file.issuer_pem.content}"
  }
  
  set {
    name = "cert.key"
    value = local_file.key_pem.content
  }
}


#resource "kubernetes_manifest" "piholeadminpassword" {
#  manifest = {
#    apiVersion = "kubernetes-client.io/v1"
#    kind = "ExternalSecret"
#    metadata = {
#      name = "piholepw"
#      namespace = "pihole"
#    }
#    spec = {
#      backendType = "secretsManager"
#    } 
#    data = [{
#      key = "hello-service/password"
#      name = "password"
#    }]
#  }
#}

resource "random_password" "piholepw" {
  length = 16
  special = false
}

resource "kubernetes_secret" "piholepw" {
  metadata {
    name = "piholepw"
    namespace = "pihole"
  }
  data = {
    password = random_password.piholepw.result
  }
}

