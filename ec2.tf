

module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "4.0.0"

  name = "pihole-instance"

  ami                    = "ami-0510e632102e49c91"
  instance_type          = "m5a.large"
  key_name               = aws_key_pair.piholekey.key_name
  monitoring             = false
  vpc_security_group_ids = [module.sg.security_group_id]
  subnet_id              = module.vpc.public_subnets[0]
  
  create_spot_instance = true

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

resource "aws_eip" "eip" {
  depends_on = [module.ec2_instance]
  instance = module.ec2_instance.spot_instance_id
  vpc      = true
}

resource "aws_route53_record" "dns_record" {
  zone_id = var.dns_zone_id
  name    = var.dns_hostname
  type    = "A"
  records = [aws_eip.eip.public_ip]
  ttl = 30
}