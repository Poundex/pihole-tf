
provider "aws" {
  region = "eu-west-2"
  profile = "pxpx"
}

provider "kubernetes" {
  config_path = "kubeconfig"
}

provider "helm" {
  kubernetes {
    config_path = "kubeconfig"
  }
}

terraform {
  #  required_version = "~> 1.0.3"

  required_providers {
    acme = {
      source  = "vancluever/acme"
      version = "~> 2.5.3"
    }
  }
}

provider "acme" {
  ## Live provider has limit 5 issues over 7 days, use staging for testing

  ## STAGING PROVIDER
#  server_url = "https://acme-staging-v02.api.letsencrypt.org/directory"

  ##LIVE PROVIDER
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
}